#!/usr/bin/python3.6
#######
# Autor: Ognjen Stanisavljevic & Srdjan Roksandic
# Datum pravljenja: 13.05.2019
# Datum modifikacije: 23.09.2019
# Verzija: 3.1.5
#######
#
# Morao sam da instaliram: pip install cryptography==2.4.2
#
# 1. Mora da se napravi 3. fodlera: skladiste, scripts i log
# 2. Skriptu staviti u ./script i tu napravite 2 file: hosts i qnap-host. U hosts stavljate IP adrese sa mikrotikovima a u qnap-host ip adrese od qnapa
# 3. U log folder postavljate file mt-backup.log
# 4. Napraviti user cfg-server na mikrotikovima i na qnap-u
# 5. Obavezno uraditi ssh-copy-id ka qnap-u da ne bi trazio sifru i importovati ssh public id na svim mikrotikovima za user-a cfg-server
# 6. U crontab-u staviti: 30 17 * * * cd /home/$USER/produkcija/scripts/ && ./mt_backup.py
#
# Relativna putanja: BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 
# Verzija: 1.2.5
#
#######

from datetime import datetime
import paramiko, os, time, zipfile, tarfile, smtplib, email
from email.mime.multipart  import MIMEMultipart
from email.mime.text  import MIMEText

ssh=paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
today = datetime.now()
mt_username='cfg-server'
#rsa_private_key = r'/home/paramikouser/.ssh/rsa_private_key'
path_remote = '/share/CACHEDEV9_DATA/servisi/'+ today.strftime('%Y')+ '/'+ today.strftime('%b')
path_local = '../skladiste/'
path_log= '../log/mt-backup.log'

def mejl(tekst):
  fromaddr = 'python@matijevicdoo.com'
  toaddr = 'itsluzba@matijevicdoo.com'
  msg = MIMEMultipart()
  msg['From'] = fromaddr
  msg['To'] = toaddr
  msg['Subject'] = 'Python email'

  body = tekst
  msg.attach(MIMEText(body, 'plain'))

  server = smtplib.SMTP('mail.matijevicdoo.com', 587)
  server.ehlo()
  server.starttls()
  server.ehlo()
  server.login('python@matijevicdoo.com', 'SifraZaMailNalog')
  text = msg.as_string()
  server.sendmail(fromaddr, toaddr, text)
  return

def log_upis(tekst):
  cre_file_mikrotik_log = open(path_log, 'a+')
  cre_file_mikrotik_log.write(today.strftime('\n%Y-%m-%d %X ')+tekst)
  cre_file_mikrotik_log.close()
  return

def backup_download(tip, tm):
  while True:
    tm=tm+1
    if tm < 12:
      try:
        transfer.get(hostname+tip, path_local+hostname+tip)
      except:
        time.sleep(5)
        continue
      else:
        log_upis('Fajl %s%s je skinut iz: %d. puta' % (hostname, tip, tm))
        break
    else:
      log_upis('%s%s fajl nije prebacen' % (hostname, tip))
      mejl('%s%s fajl nije prebacen' % (hostname, tip))
      break    
  return

def brisanje_svega():
  os.system('rm ../skladiste/*')
  os.remove('../MT-'+today.strftime('%Y-%m-%d')+'.tar.gz')
  return

cre_file_mikrotik_log = open(path_log, 'a+')
cre_file_mikrotik_log.write('========================================='+today.strftime('%Y-%m-%d %H:%M')+'=========================================')
cre_file_mikrotik_log.close()

#log - pravljenje fajla i ubacivanje datuma pocetka

stavljanje = open('qnap-host', 'r')
for nas in stavljanje:
  try: 
    ssh.connect(nas, username=mt_username, port=3388)
  except: 
    log_upis('Konekcija sa QNAP-om nije uspela, proveri ssh kljuc\n')
    ssh.close()  
    exit()
ssh.close()    
stavljanje.close()

# Uspostavljanje konekcije ka mikrotikovima i uzmanje backup-a
uzimanje = open('hosts', 'r',)
for ip in uzimanje:
  try:
    ssh.connect(ip, username=mt_username)
  except:
    log_upis('Konekcija ka %s nije dostupna' % ip)
    mejl('Konekcija ka %s nije dostupna' % ip)
    continue
  #staviti sa kojim ip je ostvarena konekcija u print i log
  else:
    log_upis('Konekcija je ostvarena sa adresom %s' % ip.strip())
    stdin, stdout, stderr = ssh.exec_command('/system identity print')
    hostnamesrz=(stdout.read().decode('utf-8'))
    hostname = hostnamesrz[hostnamesrz.find(': ')+2:].strip()
    ssh.exec_command('export file='+hostname+'.rsc')
    time.sleep(5)
    ssh.exec_command('system backup save name='+hostname+'.backup')
    #fajlovi se kreiraju log
    log_upis('Fajlovi se kreiraju.')
    time.sleep(5)
    transfer = ssh.open_sftp()
    backup_download('.rsc', 0)
    time.sleep(2)
    backup_download('.backup', 0) 
    #sftp je preuzeo fajlove log
    log_upis('Fajlovi su preuzeti sa %s' % hostname)
    transfer.close()
    ssh.exec_command('file remove '+hostname+'.rsc')
    time.sleep(0.5)
    ssh.exec_command('file remove '+hostname+'.backup')
    log_upis('Fajlovi su izbrisani sa mikrotika')
    ssh.close()  
uzimanje.close()
# Pauzira skriptu na 1 sekund
time.sleep(1)

def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, 'w:gz') as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))
make_tarfile('../MT-'+today.strftime('%Y-%m-%d')+'.tar.gz', '../skladiste/')
#pravi se zip log
log_upis('Fajlovi su zipovani')

time.sleep(5)
# Uspostavlja konekciju sa QNAP-om i prebacuje backup-e u folder

stavljanje = open('qnap-host', 'r')
for nas in stavljanje:
  ssh.connect(nas, username=mt_username, port=3388)
  #konekcija sa QNAP log
  log_upis('Uspostavljena je konekcija sa QNAP-om: %s' % nas.strip())
  ssh.exec_command('mkdir -p '+ path_remote)
  #kreiranje puta na qnapu log
  log_upis('Folder na QNAP-u je kreiran u koliko nije postojao')
  transfer = ssh.open_sftp()
  transfer.put( '../MT-'+today.strftime('%Y-%m-%d')+'.tar.gz', path_remote+'/MT-'+today.strftime('%Y-%m-%d-%H-%M')+'.tar.gz', callback=None, confirm=True)
  #fajlovi su prebaceni na qnap log
  log_upis('Zipovani fajl je prebacen na QNAP')
  transfer.close()
  time.sleep(2)
  ssh.close()
stavljanje.close()
brisanje_svega()
time.sleep(5)
log_upis('Fajlovi se obrisani sa servera\n')
#fajlovi su obrisani log
exit()
