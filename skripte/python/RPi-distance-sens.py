#!/usr/bin/python3.7
#######
# Autor: Ognjen Stanisavljevic
# Datum pravljenja: 17.08.2019
# Datum modifikacije: 02.10.2019
# Verzija: 1.1.5
#######

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

TRIG = 4
ECHO = 18

GPIO.setup(TRIG, GPIO.OUT)
GPIO.setup(ECHO, GPIO.IN)

GPIO.output(TRIG, True)
time.sleep(0.00001)
GPIO.output(TRIG, False)

while GPIO.input(ECHO) == False:
    start = time.time()

while GPIO.input(ECHO) == True:
    end = time.time()

sig_time = end-start

#Centimetres:
distance_cm = sig_time / 0.000058
distance_m = distance_cm / 100

print('Distance: {} cm'.format(distance_cm))
print('Distance: {} m'.format(distance_m))


GPIO.cleanup()