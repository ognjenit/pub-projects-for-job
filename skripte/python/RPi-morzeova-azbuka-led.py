#!/usr/bin/python3.7
#######
# Author: Ognjen Stanisavljevic
# Create date: 15.09.2019
# Modification date: 30.03.2020
# Version: 1.0.5
#######

import RPi.GPIO as GPIO
from time import sleep as ts

GPIOnum=21
ps=0.5

def t1():
    i=0
    while i<1:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.1)
        GPIO.output(GPIOnum, 0)
        ts(ps)
        GPIO.cleanup()
        i=i+1
def t2():
    i=0
    while i<2:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.1)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <2:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def t3():
    i=0
    while i<3:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.1)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <3:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def t4():
    i=0
    while i<4:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.1)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <4:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def t5():
    i=0
    while i<5:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.1)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <5:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def c1():
    i=0
    while i<1:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.3)
        GPIO.output(GPIOnum, 0)
        ts(ps)
        i=i+1
        GPIO.cleanup()
def c2():
    i=0
    while i<2:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.3)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <2:
            ts(0.1)
        if i ==2:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def c3():
    i=0
    while i<3:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.3)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <3:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def c4():
    i=0
    while i<4:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.3)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <4:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()
def c5():
    i=0
    while i<5:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(GPIOnum, GPIO.OUT)
        GPIO.output(GPIOnum, 1)
        ts(0.3)
        GPIO.output(GPIOnum, 0)
        i=i+1
        if i <5:
            ts(0.1)
        else:
            ts(ps)
            GPIO.cleanup()
            break
        GPIO.cleanup()

def razmak():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIOnum, GPIO.OUT)
    GPIO.output(GPIOnum, 0)
    ts(ps)
    GPIO.cleanup()
def A():
    t1()
    c1()
def B():
    c1()
    t3()
def C():
    c1()
    t1()
    c1()
    t1()
def D():
    c1()
    t2()
def E():
    t1()
def F():
    t2()
    c1()
    t1()
def G():
    c2()
    t1()
def H():
    t4()
def I():
    t2()
def J():
    t1()
    c3()
def K():
    c1()
    t1()
    c1()
def L():
    t1()
    c1()
    t2()
def M():
    c2()
def N():
    c1()
    t1()
def O():
    c3()
def P():
    t1()
    c2()
    t1()
def Q():
    c2()
    t1()
    c1()
def R():
    t1()
    c1()
    t1()
def S():
    t3()
def T():
    c1()
def U():
    t2()
def V():
    t3()
    c1()
def W():
    t1()
    c2()
def X():
    c1()
    t2()
    c1()
def Y():
    c1()
    t1()
    c2()
def Z():
    c2()
    t2()
def 0():
    c5()
def 1():
    t1()
    c4()
def 2():
    t2()
    c3()
def 3():
    t3()
    c2()
def 4():
    t4()
    c1()
def 5():
    t5()
def 6():
    c1()
    t4()
def 7():
    c2()
    t3()
def 8():
    c3()
    t2()
def 9():
    c4()
    t1()

poruka = input('Write message:').upper()
for slovo in poruka:
    if slovo == ' ':
        razmak()
    elif slovo.isalnum() is True:
        eval(slovo+'()')
    else:
        print('You can use a-z, 0-9 and space.')