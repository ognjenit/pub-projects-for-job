#!/usr/bin/env python3.8
#######
# Autor: Ognjen Stanisavljevic
# Datum pravljenja: 08.10.2019
# Datum modifikacije: 15.11.2019
# Verzija: 1.0.1
#######
import mysql.connector

db = mysql.connector.connect(
    host="172.16.240.61",
    user="root",
    passwd="Some32Pas$word!",
    auth_plugin='mysql_native_password',
    db="stanisavljevic")
# If u get error: Authentication plugin 'caching_sha2_password' is not supported
# this is becouse mysql 8.0 is change rules and you need to do next comamnd in mysql:
# ALTER USER 'root'@'localhost' IDENTIFIED BY 'password' PASSWORD EXPIRE NEVER;
# ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '{NewPassword}';

# Create a Cursor object to execute queries.
cur = db.cursor()

# Select data from table using SQL query.
cur.execute("SELECT * FROM informacije_koren")

# print the first column
for row in cur.fetchall() :
    print(row[0])
