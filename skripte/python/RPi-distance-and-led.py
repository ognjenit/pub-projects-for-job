#!/usr/bin/python3.7
#######
# Author: Ognjen Stanisavljevic
# Create date: 24.08.2019
# Modification date: 28.03.2020
# Version: 1.0.8
#######

import RPi.GPIO as GPIO
import time, subprocess

GPIO.setwarnings(False)
GPIO.cleanup()

GPIO.setmode(GPIO.BCM)
TRIG = 4
ECHO = 18

RED = 17
YELLOW = 27
GREEN = 22

GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)

GPIO.setup(GREEN,GPIO.OUT)
GPIO.setup(YELLOW,GPIO.OUT)
GPIO.setup(RED,GPIO.OUT)


def green_light():
    GPIO.output(GREEN, GPIO.HIGH)
    GPIO.output(YELLOW, GPIO.LOW)
    GPIO.output(RED, GPIO.LOW)

def yellow_light():
    GPIO.output(GREEN, GPIO.LOW)
    GPIO.output(YELLOW, GPIO.HIGH)
    GPIO.output(RED, GPIO.LOW)

def red_light():
    GPIO.output(GREEN, GPIO.LOW)
    GPIO.output(YELLOW, GPIO.LOW)
    GPIO.output(RED, GPIO.HIGH)

def nothing_light():
    GPIO.output(GREEN, GPIO.LOW)
    GPIO.output(YELLOW, GPIO.LOW)
    GPIO.output(RED, GPIO.LOW)

def get_distance():


    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)

    while GPIO.input(ECHO) == False:
        start = time.time()

    while GPIO.input(ECHO) == True:
        end = time.time()

    sig_time = end-start

    #CM:
    distance = sig_time / 0.000058
    distance = int(distance)

    #inches:
    distance_inch = sig_time / 0.000148
    distance_inch = int(distance_inch)

    return distance, distance_inch

try:
    while True:
        distance,distance_inch = get_distance()
        time.sleep(0.2)
        subprocess.run(['clear'])
        print(
f"""
####################################
######### Distance cm:   {distance}
######### Distance inch: {distance_inch}
###################################
""")
        if distance >= 20 and distance < 60:
            green_light()
        elif distance < 20 and distance > 10:
            yellow_light()
        elif distance <= 10:
            red_light()
        else:
            nothing_light()
except:
    nothing_light()
    GPIO.setwarnings(False)
    GPIO.cleanup()
    subprocess.run(['clear'])
    print('Script has been stoped.')
    exit()
