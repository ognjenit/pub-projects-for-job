#!/bin/bash
#######
# Autor: Ognjen Stanisavljevic
# Datum pravljenja: 28.01.2019
# Datum modifikacije: 11.03.2019
# Verzija: 1.0.6
#######
# Skripta pokazuje mogucnosti kako izvlaciti odrecene informacije is STDOUT i kristiti za proveru
#######

ok=$( uname -r |awk -F '-' '{ print $1 }' )
ok1=$( uname -r |awk -F '-' '{ print $1 }' |awk -F '.' '{print $1}' )
ok2=$( uname -r |awk -F '-' '{ print $1 }' |awk -F '.' '{print $2}' )
ok3=$( uname -r |awk -F '-' '{ print $1 }' |awk -F '.' '{print $2}' )
ok4=$( uname -r |awk -F '-' '{ print $2 }' |awk -F '.' '{print $1}' )
ok5=$( uname -r |awk -F '-' '{ print $2 }' |awk -F '.' '{print $2}' )
ok6=$( uname -r |awk -F '-' '{ print $2 }' |awk -F '.' '{print $3}' )
nk=$( yum info kernel |grep Version |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 )
nk1=$( yum info kernel |grep Version |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 |awk -F '.' '{print $1}' )
nk2=$( yum info kernel |grep Version |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 |awk -F '.' '{print $2}' )
nk3=$( yum info kernel |grep Version |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 |awk -F '.' '{print $3}' )
nk4=$( yum info kernel |grep Release |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 |awk -F '.' '{print $1}' )
nk5=$( yum info kernel |grep Release |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 |awk -F '.' '{print $2}' )
nk6=$( yum info kernel |grep Release |tail -n 1 |awk -F ':' '{ print $2 }' |cut  -d ' ' -f 2 |awk -F '.' '{print $3}' )
ker1="yum install kernel"

if [ "$nk1" -gt "$ok1" ]
then
        clear
        echo "#####################################################"
        echo "#"
        echo "# Old kernel is: $ok-$ok4.$ok5.$ok6"
        echo "# New kernel is: $nk-$nk4.$ok5.$ok6"
        echo "#"
        echo "# The script is activated becouse it is $nk1 more then $ok1."
        echo "#"
        echo "#####################################################"
        echo "Kernel upgrade is here, do you want upgrede?"
        read -p "If you want, next command will be done: $ker1. [y/n]: " -n 1 -r
                if [[ $REPLY =~ ^[Yy]$ ]]
                then
                        echo "."
                        echo "Kernel will be upgraded.."
                        $ker1
                else
                        echo "."
                        echo "Bad answare"
                fi

else
        if [ "$nk2" -gt "$ok2" ]
        then
                clear
                echo "#####################################################"
                echo "#"
                echo "# Old kernel is: $ok-$ok4.$ok5.$ok6"
                echo "# New kernel is: $nk-$nk4.$ok5.$ok6"
                echo "#"
                echo "# The script is activated becouse it is $nk2 more then $ok2."
                echo "#"
                echo "#####################################################"
                echo "Kernel upgrade is here, do you want upgrede?"
                read -p "If you want, next command will be done: $ker1. [y/n]: " -n 1 -r
                        if [[ $REPLY =~ ^[Yy]$ ]]
                        then
                                echo "."
                                echo "Kernel will be upgraded.."
                                $ker1
                        else
                                echo "."
                                echo "Bad answare"
                        fi
        else
                if [ "$nk3" -gt "$ok3" ]
                then
                        clear
                        echo "#####################################################"
                        echo "#"
                        echo "# Old kernel is: $ok-$ok4.$ok5.$ok6"
                        echo "# New kernel is: $nk-$nk4.$ok5.$ok6"
                        echo "#"
                        echo "# The script is activated becouse it is $nk3 more then $ok3."
                        echo "#"
                        echo "#####################################################"
                        echo "Kernel upgrade is here, do you want upgrede?"
                        read -p "If you want, next command will be done: $ker1. [y/n]: " -n 1 -r
                                if [[ $REPLY =~ ^[Yy]$ ]]
                                then
                                        echo "."
                                        echo "Kernel will be upgraded.."
                                        $ker1
                                else
                                        echo "."
                                        echo "Bad answare"
                                fi
                else
                        if [ "$nk4" -gt "$ok4" ]
                        then
                                clear
                                echo "#####################################################"
                                echo "#"
                                echo "# Old kernel is: $ok-$ok4.$ok5.$ok6"
                                echo "# New kernel is: $nk-$nk4.$ok5.$ok6"
                                echo "#"
                                echo "# The script is activated becouse it is $nk4 more then $ok4."
                                echo "#"
                                echo "#####################################################"
                                echo "Kernel upgrade is here, do you want upgrede?"
                                read -p "If you want, next command will be done: $ker1. [y/n]: " -n 1 -r
                                        if [[ $REPLY =~ ^[Yy]$ ]]
                                        then
                                                echo "."
                                                echo "Kernel will be upgraded.."
                                                $ker1
                                        else
                                                echo "."
                                                echo "Bad answare"
                                        fi
                        else
                                if [ "$nk5" -gt "$ok5" ]
                                then
                                        clear
                                        echo "#####################################################"
                                        echo "#"
                                        echo "# Old kernel is: $ok-$ok5.$ok5.$ok6"
                                        echo "# New kernel is: $nk-$nk5.$ok5.$ok6"
                                        echo "#"
                                        echo "# The script is activated becouse it is $nk5 more then $ok5."
                                        echo "#"
                                        echo "#####################################################"
                                        echo "Kernel upgrade is here, do you want upgrede?"
                                        read -p "If you want, next command will be done: $ker1. [y/n]: " -n 1 -r
                                                if [[ $REPLY =~ ^[Yy]$ ]]
                                                then
                                                        echo "."
                                                        echo "Kernel will be upgraded.."
                                                        $ker1
                                                else
                                                        echo "."
                                                        echo "Bad answare"
                                                fi
                                else
                                        if [ "$nk6" -gt "$ok6" ]
                                        then
                                                clear
                                                echo "#####################################################"
                                                echo "#"
                                                echo "# Old kernel is: $ok-$ok6.$ok5.$ok6"
                                                echo "# New kernel is: $nk-$nk6.$ok5.$ok6"
                                                echo "#"
                                                echo "# The script is activated becouse it is $nk6 more then $ok6."
                                                echo "#"
                                                echo "#####################################################"
                                                echo "Kernel upgrade is here, do you want upgrede?"
                                                read -p "If you want, next command will be done: $ker1. [y/n]: " -n 1 -r
                                                        if [[ $REPLY =~ ^[Yy]$ ]]
                                                        then
                                                                echo "."
                                                                echo "Kernel will be upgraded.."
                                                                $ker1
                                                        else
                                                                echo "."
                                                                echo "Bad answare"
                                                        fi
                                        else
                                        echo "Don't have upgrade for kernel."
                                        fi
                                fi
                        fi
                fi
        fi
fi
exit 0