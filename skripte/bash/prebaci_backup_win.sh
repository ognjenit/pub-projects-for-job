@echo off
rem ********************************************
rem * Autor: Ognjen Stanisavljevic             *
rem * Datum pravljenja: 15.08.2018             *
rem * Datum modifikacije: 05.01.2019           *
rem * Verzija: 1.0.3                           *
rem * Prenos backupa na FTP server za Windows  *
rem ********************************************

cls
TITLE Prenos backupa na FTP server
echo by OgnjenIT
echo.

set BLOKACIJA=C:\Backup
set FTPSERVER=147.13.14.25
set FTPUSER=informix
set FTPPASSWD=Inf2o2017#
set FTPDIR=mi-backup/iwhftomcat
set WINZIP="C:\backup\zip"
set DANAS=%date:~10,4%%date:~4,2%%date:~7,2%
rem Ako ne radi vreme onda treba ovako: %date:~-4%%date:~3,2%%date:~0,2%

set FOLDER1="C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps"
set FOLDER2="C:\iwf_admin"
set FOLDER3="C:\JDBC_drivers"
set FOLDER4="C:\Program Files\IBM"
set FOLDER5=

set TIP=zip

rem *********************************************************************************
cd %BLOKACIJA%
echo ================================================================================ >> %COMPUTERNAME%.log
date /T >> %COMPUTERNAME%.log
echo " "  >> %COMPUTERNAME%.log


for %%B in (%FOLDER1% %FOLDER2% %FOLDER3% %FOLDER4% %FOLDER5%) do (
%WINZIP% -r %COMPUTERNAME%_%DANAS%  %%B >> %COMPUTERNAME%.log
)

%WINZIP%  %COMPUTERNAME%_%DANAS% %COMPUTERNAME%.log
%WINZIP%  %COMPUTERNAME%_%DANAS% Prebaci_Backup-izmenjen-datum.cmd


rem Kreiram fajl za FTP transfer
rem copy nul > ftp.txt
echo %FTPUSER%>ftp.txt
echo %FTPPASSWD%>> ftp.txt
echo binary>> ftp.txt
echo mkdir %FTPDIR% >> ftp.txt
echo cd %FTPDIR% >> ftp.txt
echo put %COMPUTERNAME%_%DANAS%.%TIP% >> ftp.txt
echo quit >> ftp.txt

echo Kopiram fajl %COMPUTERNAME%_%DANAS%.%TIP% na FTP server %FTPSERVER% >> %COMPUTERNAME%.log
FTP -s:ftp.txt %FTPSERVER% >> %COMPUTERNAME%.log
echo " " >> %COMPUTERNAME%.log
date /T >> %COMPUTERNAME%.log
echo Zavrsen transfer >> %COMPUTERNAME%.log

del ftp.txt
rem %COMPUTERNAME%_%DANAS%.%TIP%