#!/bin/bash
#######
# Autor: Ognjen Stanisavljevic
# Datum pravljenja: 16.01.2019
# Datum modifikacije: 18.04.2019
# Verzija: 3.2.8
#######
#
# Pre samog pocetka treba instalirati let's encrypt
# git clone https://github.com/certbot/certbot.git
# skriptu staviti u isti folderu gde je i folder letsecrypt
# Kreirate file: root.pem i auto.sh
# U automatska-skripta.sh postavite ovu skriptu a u root.pem postavite sledece:
#
#-----BEGIN CERTIFICATE-----
#MIIDSjCCAjKgAwIBAgIQRK+wgNajJ7qJMDmGLvhAazANBgkqhkiG9w0BAQUFADA/
#MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
#DkRTVCBSb290IENBIFgzMB4XDTAwMDkzMDIxMTIxOVoXDTIxMDkzMDE0MDExNVow
#PzEkMCIGA1UEChMbRGlnaXRhbCBTaWduYXR1cmUgVHJ1c3QgQ28uMRcwFQYDVQQD
#Ew5EU1QgUm9vdCBDQSBYMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
#AN+v6ZdQCINXtMxiZfaQguzH0yxrMMpb7NnDfcdAwRgUi+DoM3ZJKuM/IUmTrE4O
#rz5Iy2Xu/NMhD2XSKtkyj4zl93ewEnu1lcCJo6m67XMuegwGMoOifooUMM0RoOEq
#OLl5CjH9UL2AZd+3UWODyOKIYepLYYHsUmu5ouJLGiifSKOeDNoJjj4XLh7dIN9b
#xiqKqy69cK3FCxolkHRyxXtqqzTWMIn/5WgTe1QLyNau7Fqckh49ZLOMxt+/yUFw
#7BZy1SbsOFU5Q9D8/RhcQPGX69Wam40dutolucbY38EVAjqr2m7xPi71XAicPNaD
#aeQQmxkqtilX4+U9m5/wAl0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNV
#HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFMSnsaR7LHH62+FLkHX/xBVghYkQMA0GCSqG
#SIb3DQEBBQUAA4IBAQCjGiybFwBcqR7uKGY3Or+Dxz9LwwmglSBd49lZRNI+DT69
#ikugdB/OEIKcdBodfpga3csTS7MgROSR6cz8faXbauX+5v3gTt23ADq1cEmv8uXr
#AvHRAosZy5Q6XkjEGB5YGV8eAlrwDPGxrancWYaLbumR9YbK+rlmM6pZW87ipxZz
#R8srzJmwN0jP41ZL9c8PDHIyh8bwRLtTcm1D9SZImlJnt1ir/md2cXjbDaJWFBM5
#JDGFoqgCWjBH4d1QB7wCCZAA62RjYJsWvIjJEubSfZGL+T0yjWW06XyxV3bqxbYo
#Ob8VZRzI9neWagqNdwvYkQsEjgfbKbYK7p2CNTUQ
#-----END CERTIFICATE-----
#
# Dodati u crontab-u: 1 5 * * 0 /opt/script-for-renew/automatska-skripta.sh
#
############################
################## MENJA USER #####################
root_dir=$PWD
domain='mail.stanisavljevic.org'
################## DOLE NE DIRAJ NISTA!############
ssl_zm='/opt/zimbra'
today=$(date +%Y-%m-%d)
runuser -l zimbra -c 'zmmailboxdctl stop'
mkdir -p $root_dir/log
touch $root_dir/log/renew-$today.log
touch $root_dir/log/glavni-log.log
$root_dir/letsencrypt/letsencrypt-auto renew > $root_dir/log/renew-$today.log
trenutni_odg=$( cat $root_dir/log/renew-$today.log |grep '(' |cut -d '(' -f 2 |awk -F ')' '{print $1}' )
dobar_odg='success'
#provera da ki postoji mogucnost da se uradi sertifikat. Lets encryp dozvoljava samo mesec dana pre isteka!
if [ $trenutni_odg = $dobar_odg ]
then
    mkdir $ssl_zm/ssl/letsencrypt-$today
    cp /etc/letsencrypt/live/$domain/* $ssl_zm/ssl/letsencrypt-$today/
    chown zimbra:zimbra $ssl_zm/ssl/letsencrypt-$today/*
    ls -lah $ssl_zm/ssl/letsencrypt-$today
    cp $ssl_zm/ssl/letsencrypt-$today/chain.pem $ssl_zm/ssl/letsencrypt-$today/bundle.pem
    cat $root_dir/root.pem >> $ssl_zm/ssl/letsencrypt-$today/bundle.pem
    ls -lah $ssl_zm/ssl/letsencrypt-$today/bundle.pem
    mv $ssl_zm/ssl/zimbra/commercial/commercial.key $ssl_zm/ssl/zimbra/commercial/commercial.key-do-$today
    cp $ssl_zm/ssl/letsencrypt-$today/privkey.pem $ssl_zm/ssl/zimbra/commercial/commercial.key
    chown zimbra:zimbra $ssl_zm/ssl/zimbra/commercial/commercial.key
    cd $ssl_zm/ssl/letsencrypt-$today/
    runuser -l zimbra -c $ssl_zm'/bin/zmcertmgr verifycrt comm '$ssl_zm'/ssl/zimbra/commercial/commercial.key '$ssl_zm'/ssl/letsencrypt-$(date +%Y-%m-%d)/cert.pem '$ssl_zm'/ssl/letsencrypt-$(date +%Y-%m-%d)/bundle.pem'
    runuser -l zimbra -c $ssl_zm'/bin/zmcertmgr deploycrt comm '$ssl_zm'/ssl/letsencrypt-$(date +%Y-%m-%d)/cert.pem '$ssl_zm'/ssl/letsencrypt-$(date +%Y-%m-%d)/bundle.pem'
    runuser -l zimbra -c 'zmmailboxdctl start'
    echo '--------$today--------' >>$root_dir/log/glavni-log.log
    echo 'odgovor=$trenutni_odg' >>$root_dir/log/glavni-log.log
    echo 'Danas je implementiran sertifikat!' >>$root_dir/log/glavni-log.log
    echo '' >>$root_dir/log/glavni-log.log
    exit 1
else
    runuser -l zimbra -c 'zmmailboxdctl start'
    rm $root_dir/log/renew-$today.log
    echo '--------'$today'--------' >>$root_dir/log/glavni-log.log
    echo 'odgovor='$trenutni_odg >>$root_dir/log/glavni-log.log
    echo 'Danas nije uspeo da postavi novi sertifikat' >>$root_dir/log/glavni-log.log
    echo '' >>$root_dir/log/glavni-log.log
    exit 1
fi
